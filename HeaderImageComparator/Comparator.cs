﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeaderImageComparator
{
    static class Comparator
    {
        public static void CompareImages(string[] tempFiles)
        {
            Console.WriteLine("Start Analyzing Files");
            Console.WriteLine("Number of files: " + tempFiles.Length);

            List<Color[]> tempColorList = ImageAnalyzer.Analyze(tempFiles, BaseValues.SampleHeight, BaseValues.LeftOffset);

            Console.WriteLine("Start Actual Compare");

            List<Tuple<int, int>> duplicates = Compare(tempColorList);

            Console.WriteLine("-------------");

            foreach (var duplicate in duplicates)
            {
                Console.WriteLine("Possibe Duplicates: " + tempFiles[duplicate.Item1] + " - " + tempFiles[duplicate.Item2]);
                File.AppendAllText("Duplicate.txt", "Possibe Duplicates: " + tempFiles[duplicate.Item1] + " - " + tempFiles[duplicate.Item2] + Environment.NewLine);
            }

            Console.WriteLine("-------------");
            Console.WriteLine("Finished");
        }

        public static List<Tuple<int, int>> Compare(List<Color[]> tempColorList)
        {
            List<Tuple<int, int>> duplicates = new List<Tuple<int, int>>();

            for (int i = 0; i < tempColorList.Count; i++)
            {
                for (int j = i + 1; j < tempColorList.Count; j++)
                {
                    if (ArrayComparator.CompareColorArray(tempColorList[i], tempColorList[j], 0))
                    {
                        duplicates.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            return duplicates;
        }


    }
}
