﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeaderImageComparator
{
    static class ArrayComparator
    {
        public static bool CompareIntArray(int[] tempArray1, int[] tempArray2, int comparatorOffset)
        {
            if (tempArray1 == null || tempArray2 == null)
            {
                return false;
            }

            if (tempArray1.Length != tempArray2.Length)
            {
                return false;
            }

            for (int i = 0; i < tempArray1.Length; i++)
            {
                //Checking if our array values match  (using the offset as error), if it doesn't match we break
                if(!(tempArray1[i] < (tempArray2[i] + comparatorOffset) || tempArray1[i] > (tempArray2[i] - comparatorOffset)))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool CompareColorArray(Color[] tempArray1, Color[] tempArray2, int comparatorOffset)
        {
            if (tempArray1 == null || tempArray2 == null)
            {
                return false;
            }

            if (tempArray1.Length != tempArray2.Length)
            {
                return false;
            }

            for (int i = 0; i < tempArray1.Length; i++)
            {
                //Checking if our array values match  (using the offset as error), if it doesn't match we break
                if ((tempArray1[i].R < (tempArray2[i].R + comparatorOffset) || tempArray1[i].R > (tempArray2[i].R - comparatorOffset)))
                {
                    return false;
                }

                if ((tempArray1[i].G < (tempArray2[i].G + comparatorOffset) || tempArray1[i].G > (tempArray2[i].G - comparatorOffset)))
                {
                    return false;
                }

                if ((tempArray1[i].B < (tempArray2[i].B + comparatorOffset) || tempArray1[i].B > (tempArray2[i].B - comparatorOffset)))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
