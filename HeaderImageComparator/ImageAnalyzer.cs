﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeaderImageComparator
{
    static class ImageAnalyzer
    {
        /*
         * This Method starts top left, then goes the LeftOffset distance to the right,
         * then it goes down, looking for non empty pixels (declared in BaseValues),
         * if it finds a series of them (sampleHeight) it returns them, otherwise it goes on to the next row
         */

        public static List<Color[]> Analyze(string[] tempFiles, int sampleHeight, int leftOffset)
        {
            List<Color[]> tempColorList = new List<Color[]>();

            for (int i = 0; i < tempFiles.Length; i++)
            {
                Color[] tempColor = GetSampleLeftRight(new Bitmap(tempFiles[i]), sampleHeight, leftOffset);
                tempColorList.Add(tempColor);
            }

            return tempColorList;
        }

        public static Color[] GetSampleLeftRight(Bitmap tempBitmap, int sampleHeight, int leftOffset)
        {
            Color[]  tempColorList = new Color[sampleHeight];
            
            for (int i = leftOffset; i < tempBitmap.Width; i++)
            {
                int currentSampleHeight = 0;

                for (int j = 0; j < tempBitmap.Height; j++)
                {
                    Color tempColor = tempBitmap.GetPixel(i, j);

                    if (tempColor.R < (BaseValues.IgnoreColorR - BaseValues.IgnoreColorOffset) ||
                        tempColor.R > (BaseValues.IgnoreColorR + BaseValues.IgnoreColorOffset) ||
                        tempColor.G < (BaseValues.IgnoreColorG - BaseValues.IgnoreColorOffset) ||
                        tempColor.G > (BaseValues.IgnoreColorG + BaseValues.IgnoreColorOffset) ||
                        tempColor.B < (BaseValues.IgnoreColorB - BaseValues.IgnoreColorOffset) ||
                        tempColor.B > (BaseValues.IgnoreColorB + BaseValues.IgnoreColorOffset))
                    {
                        tempColorList[currentSampleHeight] = tempColor;
                        currentSampleHeight++;

                        if (currentSampleHeight == sampleHeight)
                        {
                            return tempColorList;
                        }
                    }
                    else
                    {
                        currentSampleHeight = 0;
                    }
                }                
            }

            return null;
        }
    }
}
