﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeaderImageComparator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting");

            string[] filenames = Directory.GetFiles(".", "*.jpg");
            
            Comparator.CompareImages(filenames);

            Console.ReadLine();
        }
    }
}
