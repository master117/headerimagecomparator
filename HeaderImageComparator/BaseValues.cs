﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeaderImageComparator
{
    static class BaseValues
    {
        public static Byte IgnoreColorR = 255;
        public static Byte IgnoreColorG = 255;
        public static Byte IgnoreColorB = 255;
        public static Byte IgnoreColorOffset = 3;

        public static int SampleHeight = 15;
        public static int LeftOffset = 600;

        public static int ArrayComparatorOffset = 2;
    }
}
